#!/bin/bash

go get github.com/favadi/protoc-go-inject-tag@v1.1.0
go get github.com/golang/protobuf@v1.4.2

protobuf=${GOPATH}/pkg/mod/github.com/golang/protobuf@v1.4.2
inject_tag=${GOPATH}/bin/protoc-go-inject-tag
cur_dir=$(pwd)

generateGRpc() {
  grpc_dest_dir=$1
  proto_file=${2}.proto
  pb_file_name=${2}.pb.go
  pb_file_path=${cur_dir}/${grpc_dest_dir}/${pb_file_name}

  echo "开始生成pb文件 ${proto_file}"
  protoc -I "${1}"/ \
    -I "${protobuf}" \
    --go-grpc_out="${grpc_dest_dir}" \
    --go-grpc_opt=paths=source_relative \
    --go_out="${grpc_dest_dir}" \
    --go_opt=paths=source_relative "${proto_file}"

  echo "重新生成json标签 ${pb_file_name}"
  ${inject_tag} -input="${pb_file_path}"
}

generateGRpc resprpc resp_msg
