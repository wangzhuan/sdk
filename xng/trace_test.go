package xng

import (
	"context"
	"sync"
	"testing"
)

func TestRandStringBytes(t *testing.T) {
	count := 0
	m := make(map[string]struct{})


	for index := 0;index < 1000000;index++ {
		s := GetJaegerTraceID()
		_, exists := m[s]
		if exists {
			count ++
			continue
		}
		m[s] = struct{}{}
	}
	t.Log(count)
}

func A(ctx context.Context) {
	m := &sync.Map{}
	m.Store("trace","a")
	ctx = context.WithValue(ctx,"map",m)
}
func B(ctx context.Context) {
	m := ctx.Value("map")
	sm, ok := m.(*sync.Map)
	if !ok {
		return
	}
	l, ok := sm.Load("1")
	if !ok {
		return
	}
	s ,ok := l.(string)
	if !ok {
		return
	}
	_ = s
}

