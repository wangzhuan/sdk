package xng

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net"
	"time"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap/zapcore"
)

var LocalIp string

func init() {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		fmt.Errorf("Oops, InterfaceAddrs err:\n%v", err)
	}
	for _, a := range addrs {
		if ipNet, ok := a.(*net.IPNet); ok && !ipNet.IP.IsLoopback() {
			if ipNet.IP.To4() != nil {
				LocalIp = ipNet.IP.String()
				return
			}
		}
	}
}

/*
{
    trace: {
        from:  // trace来源，如某个业务的某个模块
        step:  // trace深度，请求到第几层了
        id:    // trace id，每次请求都有的一个唯一的id，比如可以采用每次生成不重复的UUID
    },
    addr: {
        rmt:  // 对端的ip和端口，比如10.2.3.4:8000
        loc:  // 本地的ip和端口，比如10.2.3.5:8020
    },
    time: // 毫秒 201809221830100 「global」
    params:{} // 请求参数
    elapse: // 耗时，毫秒
    result: { // 返回结果
        ret: 1,
        data: {}
    },
    level: //INFO、ERROR、WARNING、DEBUG 「global」
    path: “filename.go:300:/favor/add", // 打点位置  「global」
    ext: {} // 自定义字段。不超过5个
}
*/
type bodyLogWriter struct {
	gin.ResponseWriter
	body *bytes.Buffer
}

func (w *bodyLogWriter) Write(b []byte) (int, error) {
	w.body.Write(b)
	return w.ResponseWriter.Write(b)
}

func (w bodyLogWriter) WriteString(s string) (int, error) {
	w.body.WriteString(s)
	return w.ResponseWriter.WriteString(s)
}

type addr struct {
	RemoteIP string
	LocalIP  string
}

func (a *addr) MarshalLogObject(enc zapcore.ObjectEncoder) error {
	enc.AddString("rmt", a.RemoteIP)
	enc.AddString("loc", a.LocalIP)
	return nil
}

//func getRequestParam(c *gin.Context) (param interface{}) {
//	params, exists := c.Get(KEY_PARAMS)
//	if exists {
//		return params
//	}
//
//	var body []byte
//	if cb, ok := c.Get(gin.BodyBytesKey); ok {
//		if cbb, ok := cb.([]byte); ok {
//			body = cbb
//		}
//	}
//
//	if body != nil {
//		length := len(body)
//		param = string(body[0:length])
//	}
//	return
//}

//func getResponse(c *gin.Context) (result interface{}) {
//	result, ok := c.Get(KEY_RESULT)
//	if !ok {
//		result = "Boss default result"
//	}
//	return
//}

type BossParam struct {
	FilterURLPath      []string //屏蔽的URLPath 不进行日志打印
	MaxReqBodyByteSize int      //reqBody打印的最大值
}

var (
	DefaultBossParam = &BossParam{
		FilterURLPath:      []string{"/health", "/metrics"},
		MaxReqBodyByteSize: 3145728, //3M
	}
)

//Boss 链路函数 param 为nil会用默认配置DefaultBossParam
func Boss(serverName string, logger TraceLogger, param *BossParam) gin.HandlerFunc {
	return func(c *gin.Context) {
		// Start timer
		start := time.Now()
		traceInfo := NewTraceInfo(c, c.Request, serverName)

		// url path
		urlPath := c.FullPath()
		if urlPath == "" {
			urlPath = c.Request.URL.Path
		}

		// filter path
		if param == nil {
			param = DefaultBossParam
		}
		for _, path := range param.FilterURLPath {
			if urlPath == path {
				c.Next()
				return
			}
		}

		clientIP := c.ClientIP()
		reqBuf, _ := ioutil.ReadAll(c.Request.Body)
		reqBuf1 := ioutil.NopCloser(bytes.NewBuffer(reqBuf))
		reqBuf2 := ioutil.NopCloser(bytes.NewBuffer(reqBuf))
		c.Request.Body = reqBuf2
		reqBody, _ := ioutil.ReadAll(reqBuf1)

		blw := &bodyLogWriter{body: bytes.NewBufferString(""), ResponseWriter: c.Writer}
		c.Writer = blw

		// Process request
		c.Next()

		//params := getRequestParam(c)
		//result := getResponse(c)

		// TODO: URLPath需要组装到logger的caller（即path）上。
		// Note：需要访问：ce.Entry.Caller。zap没有返回ce，而且ce是在调用Logger.Info时的check函数里才产生的。
		// 或许修改一下wiki的path的结构规范可以很好解决，把具体的请求route url独立出来，否则解法比较hack蛋疼，增加一些重复无用代码

		// 以 path: “filename.go:300:/favor/add" 为例暂时拆分为：
		// {
		// 		caller：“filename.go:300"
		//		url: "/favor/add"
		//  }

		_addr := &addr{
			RemoteIP: clientIP,
			LocalIP:  LocalIp,
		}
		traceInfo.StartTime = start.UnixNano() / int64(time.Microsecond)

		// Stop timer
		end := time.Now()
		latency := end.Sub(start)

		if len(reqBody) > param.MaxReqBodyByteSize {
			reqBody = reqBody[:param.MaxReqBodyByteSize]
		}

		serviceLevel := GetServiceLevel(c, true)
		retCode, _ := GetRet(c)

		m := map[string]interface{}{
			"addr":         _addr,
			"serviceLevel": serviceLevel,
			"retCode":      retCode,
			"elapse":       latency.Nanoseconds() / int64(time.Millisecond),
			"url":          urlPath,
			"status":       c.Writer.Status(),
			"body":         string(reqBody),
			"response":     blw.body.String(),
			TraceID:        traceInfo.TraceID,
			"traceInfo":    traceInfo,
		}

		if urlPath != c.Request.RequestURI {
			m["ori_url"] = c.Request.RequestURI
		}

		logger.InfoW("jaeger-trace", m)
	}
}
