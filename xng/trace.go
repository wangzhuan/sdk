package xng

import (
	"context"
	"fmt"
	"math/rand"
	"net/http"
	"strconv"
	"sync"
	"time"
	"unsafe"

	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
	"gitlab.com/wangzhuan/sdk/lib"
	"gitlab.com/wangzhuan/utils"
)

// SampleBaseRate 采样比率
const SampleBaseRate = 100
const letterBytes = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

const (
	letterIdxBits = 6                    // 6 bits to represent a letter index
	letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
	letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bit
)

// FullTraceKeyType 类型
type FullTraceKeyType string

// FullTraceKey 保存在context中的key值
const FullTraceKey FullTraceKeyType = "full-trace-key"

var pool *sync.Pool

// GetRandTraceID 获得随机TraceID 参数：n 想要字符串长度
func GetRandTraceID(n int) string {
	b := make([]byte, n)
	for i, cache, remain := n-1, rand.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = rand.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterBytes) {
			b[i] = letterBytes[idx]
			i--
		}
		cache >>= letterIdxBits
		remain--
	}
	return *(*string)(unsafe.Pointer(&b))
}

// GetJaegerTraceID 获得jaeger格式的traceID
func GetJaegerTraceID() string {
	generator := pool.Get().(rand.Source)
	number := uint64(generator.Int63())
	pool.Put(generator)

	id := fmt.Sprintf("%x", number)
	return id
}

// RefType span之间的关系 Child_Of 父子关系，Follows_From 兄弟关系
type RefType int

const (
	//RefTypeChildOf 父子关系
	RefTypeChildOf RefType = iota
	//RefTypeFollowsFrom 兄弟关系
	RefTypeFollowsFrom
)

// TraceType trace类型
type TraceType int

const (
	//TraceTypeBossSpan boss 中的span
	TraceTypeBossSpan TraceType = iota + 1
	//TraceTypeFinishSpan 服务内部span
	TraceTypeFinishSpan
)

// TraceInfo trace打印格式
type TraceInfo struct {
	TraceID      string    `json:"traceID"`
	StartTime    int64     `json:"startTime"`
	ServiceName  string    `json:"serviceName"`
	SpanID       string    `json:"spanID"`
	ParentSpanID string    `json:"pSpanId"`
	Sampler      string    `json:"sampler"`
	Type         TraceType `json:"type"`
	RefType      RefType   `json:"refType"`
}

type XHeader interface {
	Get(key string) string
}

func TraceInfoWith(header XHeader) (*TraceInfo, *sync.Map) {
	traceInfo := &TraceInfo{}
	m := &sync.Map{}
	for _, k := range lib.TraceHeaders {
		v := header.Get(k)
		switch k {
		case lib.XB3TraceID:
			if v == "" {
				v = GetJaegerTraceID()
			}
			traceInfo.TraceID = v
		case lib.XB3ParentSpanID:
			traceInfo.ParentSpanID = v
		case lib.XB3Sampled:
			if v == "" {
				v = strconv.Itoa(rand.Intn(SampleBaseRate))
			}
			traceInfo.Sampler = v
		default:
			break
		}
		m.Store(k, v)
	}
	if traceInfo.ParentSpanID == "" {
		m.Store(lib.XB3SpanID, traceInfo.TraceID)
		traceInfo.SpanID = traceInfo.TraceID
	} else {
		spanID := GetJaegerTraceID()
		m.Store(lib.XB3SpanID, spanID)
		traceInfo.SpanID = spanID
	}
	traceInfo.Type = TraceTypeBossSpan
	traceInfo.RefType = RefTypeChildOf
	return traceInfo, m
}

// NewTraceInfo 从http请求中获取trace 信息，并保存在context中
func NewTraceInfo(ctx context.Context, request *http.Request, serverName string) *TraceInfo {
	traceInfo, m := TraceInfoWith(request.Header)

	traceInfo.ServiceName = serverName
	m.Store(lib.TraceHeaderServerName, serverName)
	SetMap(ctx, m)
	return traceInfo
}

// SetMap 将sync.Map注入到context中，会对gin.context进行特殊处理
func SetMap(ctx context.Context, m *sync.Map) context.Context {
	if ctx == nil {
		return nil
	}

	switch c := ctx.(type) {
	case *gin.Context:
		c.Set(string(FullTraceKey), m)
	case *XContext:
		c.Set(string(FullTraceKey), m)
	default:
		ctx = context.WithValue(ctx, FullTraceKey, m)
	}

	return ctx
	//c, ok := ctx.(*gin.Context)
	//if ok {
	//	c.Set(string(FullTraceKey), m)
	//	return ctx
	//}
	//xc, ok := ctx.(*XContext)
	//if ok {
	//	xc.Set(string(FullTraceKey), m)
	//	return ctx
	//}
	//ctx = context.WithValue(ctx, FullTraceKey, m)

	//return ctx
}

// GetMap 从context中 获取sync.Map
func GetMap(ctx context.Context) (m *sync.Map, ok bool) {
	if ctx == nil {
		return nil, false
	}

	var v interface{}
	switch c := ctx.(type) {
	case *gin.Context:
		v, _ = c.Get(string(FullTraceKey))
	case *XContext:
		v = c.Value(string(FullTraceKey))
	default:
		v = ctx.Value(FullTraceKey)
	}

	if v != nil {
		m, ok = v.(*sync.Map)
	}

	return

	//var v interface{}
	//c, ok := ctx.(*gin.Context)
	//if ok {
	//	v, ok = c.Copy().Get(string(FullTraceKey))
	//	if !ok {
	//		return
	//	}
	//} else {
	//	v = ctx.Value(FullTraceKey)
	//}
	//
	//if v != nil {
	//	m, ok = v.(*sync.Map)
	//}
	//return
}

// TraceLogger 日志打印抽象接口
type TraceLogger interface {
	InfoW(msg string, m map[string]interface{})
}

// Span 链路追踪的单位
type Span struct {
	logger     TraceLogger
	JaegerFlag string //追踪标示，例如数据库：redis,mongo,elasticsearch 标示当前span,是追踪这些相关的路径
	TraceInfo  *TraceInfo
}

// NewSpanWithContext 从context 初始化span
func NewSpanWithContext(ctx context.Context, logger TraceLogger, JaegerFlag string, refType RefType) (span *Span) {
	span = &Span{}
	span.logger = logger
	span.JaegerFlag = JaegerFlag
	span.TraceInfo = &TraceInfo{}
	m, ok := GetMap(ctx)
	if !ok {
		return
	}
	for _, k := range lib.TraceHeaders {
		v, ok := m.Load(k)
		if !ok {
			continue
		}
		s, ok := v.(string)
		if !ok {
			continue
		}
		switch k {
		case lib.XB3TraceID:
			span.TraceInfo.TraceID = s
		case lib.XB3SpanID:
			span.TraceInfo.SpanID = s
		case lib.XB3ParentSpanID:
			span.TraceInfo.ParentSpanID = s
		case lib.XB3Sampled:
			span.TraceInfo.Sampler = s
		case lib.TraceHeaderServerName:
			span.TraceInfo.ServiceName = s
		}
	}
	serverName, _ := m.Load(lib.TraceHeaderServerName)
	if s, ok := serverName.(string); ok {
		span.TraceInfo.ServiceName = s
	}

	if span.TraceInfo.TraceID == "" {
		span.TraceInfo.TraceID = GetJaegerTraceID()
	}
	//context中的 span 是当前span 的parent
	if span.TraceInfo.SpanID == "" {
		span.TraceInfo.SpanID = span.TraceInfo.TraceID
	} else {
		span.TraceInfo.ParentSpanID = span.TraceInfo.SpanID
		span.TraceInfo.SpanID = GetJaegerTraceID()
	}
	span.TraceInfo.StartTime = time.Now().UnixNano() / int64(time.Microsecond)
	span.TraceInfo.Type = TraceTypeFinishSpan
	span.TraceInfo.RefType = refType
	return span
}

// NewSpanWithParentSpan 获得另一个span,refType 指定关系
func NewSpanWithParentSpan(parentSpan *Span, logger TraceLogger, JaegerFlag string, refType RefType) *Span {
	span := &Span{}
	span.logger = logger
	span.JaegerFlag = JaegerFlag
	span.TraceInfo = &TraceInfo{}
	if parentSpan.TraceInfo.TraceID == "" {
		span.TraceInfo.TraceID = GetJaegerTraceID()
	} else {
		span.TraceInfo.TraceID = parentSpan.TraceInfo.TraceID
	}

	if parentSpan.TraceInfo.SpanID == "" {
		span.TraceInfo.SpanID = span.TraceInfo.TraceID
	} else {
		span.TraceInfo.ParentSpanID = parentSpan.TraceInfo.SpanID
		span.TraceInfo.SpanID = GetJaegerTraceID()
	}
	span.TraceInfo.Sampler = parentSpan.TraceInfo.Sampler
	span.TraceInfo.ServiceName = parentSpan.TraceInfo.ServiceName
	span.TraceInfo.StartTime = time.Now().UnixNano() / int64(time.Microsecond)
	span.TraceInfo.Type = TraceTypeFinishSpan
	span.TraceInfo.RefType = refType
	return span
}

// Finish 结束span监听
func (s *Span) Finish() {
	if s.TraceInfo.TraceID == "" {
		return
	}

	_addr := &addr{
		LocalIP: utils.LocalIp,
	}
	elapse := time.Now().UnixNano()/int64(time.Microsecond) - s.TraceInfo.StartTime
	m := map[string]interface{}{
		"addr":      _addr,
		"elapse":    elapse / 1000, //Millisecond 统一改成Millisecond 级别
		"url":       s.JaegerFlag,
		"traceInfo": s.TraceInfo,
	}

	s.logger.InfoW("jaeger-trace", m)
}

// JaegerInjectToHeader 将链路信息从context 注入到http.header中
func JaegerInjectToHeader(ctx context.Context, header http.Header) error {
	if header == nil {
		return errors.New("http.header is nil need init")
	}
	m, ok := GetMap(ctx)
	if !ok {
		return nil
	}
	var span string
	for _, k := range lib.TraceHeaders {
		v, ok := m.Load(k)
		if !ok {
			continue
		}
		s, ok := v.(string)
		if !ok {
			continue
		}
		if k == lib.XB3SpanID {
			span = s
			continue
		}
		header.Set(k, s)
	}

	header.Set(lib.XB3ParentSpanID, span)
	return nil
}

// JaegerInjectToMap 将链路信息从context注入到map中，注：map不能为空
func JaegerInjectToMap(ctx context.Context, m map[string]string) error {
	if m == nil {
		return errors.New("map is nil need init")
	}
	ctxMap, ok := GetMap(ctx)
	if !ok {
		return nil
	}
	var span string
	for _, k := range lib.TraceHeaders {
		v, ok := ctxMap.Load(k)
		if !ok {
			continue
		}
		s, ok := v.(string)
		if !ok {
			continue
		}
		if k == lib.XB3SpanID {
			span = s
			continue
		}
		m[k] = s
	}
	m[lib.XB3ParentSpanID] = span
	return nil
}

func init() {
	rand.Seed(time.Now().UnixNano())
	pool = &sync.Pool{
		New: func() interface{} {
			return rand.NewSource(rand.Int63())
		},
	}
}
