package xng

import "encoding/json"

type Codec interface {
	Encode(v interface{}) ([]byte, error)
	Decode(data []byte, v interface{}) error
}

type JSONCodec struct{}

func NewJSONCodec() *JSONCodec {
	return &JSONCodec{}
}

func (codec *JSONCodec) Encode(v interface{}) ([]byte, error) {
	return json.Marshal(v)
}

func (codec *JSONCodec) Decode(data []byte, v interface{}) error {
	return json.Unmarshal(data, v)
}
