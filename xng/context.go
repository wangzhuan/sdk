package xng

import (
	"net/http"
	"sync"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/validator/v10"
	"gitlab.com/wangzhuan/sdk/lib"
)

const (
	//KEY_PARAMS         = "params"
	//KEY_RESULT       = "result"
	//KEY_TESING       = "isTesting"
	FieldValidFailed = "字段验证不通过"
	TraceID          = "traceID"
)

// Resp 用于定义返回数据格式(json)
type XResp struct {
	Ret    lib.Code    `json:"ret"`
	Msg    string      `json:"msg,omitempty"`
	Detail string      `json:"detail,omitempty"`
	Data   interface{} `json:"data,omitempty"`
}

type XContext struct {
	*gin.Context
	keysMutex *sync.RWMutex
	XKeys     map[string]interface{} // gin.context 的keys Get/Set/Value没有保证并发安全, XContext实现了并发安全
}

func NewXContext(c *gin.Context) *XContext {
	xc := &XContext{Context: c, keysMutex: &sync.RWMutex{}}
	if v, ok := GetMap(c); ok {
		SetMap(xc, v)
		tid, _ := v.Load(lib.XB3TraceID)
		xc.Set(lib.XRequestID, tid)
	}
	return xc
}

// GetValidatedErrorField 获取验证不通过字段详细的信息
func (c *XContext) GetValidatedErrorField(err error) (detail string) {
	errs, ok := err.(validator.ValidationErrors)
	if !ok || (errs != nil && len(errs) == 0) {
		detail = err.Error()
		return
	}

	for _, e := range errs {
		detail = detail + e.StructNamespace() + FieldValidFailed
		return
	}

	return

	//validationErrs, assertionOk := err.(validator.ValidationErrors)
	//if !assertionOk {
	//	detail = err.Error()
	//	return
	//}
	//
	//if validationErrs != nil && len(validationErrs) == 0 {
	//	detail = err.Error()
	//	return
	//}
	//
	//count := 0
	//for e := range validationErrs {
	//	if count > 0 {
	//		break
	//	}
	//	detail = detail + key + FieldValidFailed
	//	count++
	//}
	//
	//fmt.Printf("%v\n", err.Error())
	//return
}

func (c *XContext) GetReqObject(to interface{}) (ok bool) {
	//c.GetTraceHeaders()
	//使用ShouldBindBodyWith 避免前置过滤器提前读取body，造成 EOF
	err := c.ShouldBindBodyWith(to, binding.JSON)
	if err != nil {
		detail := c.GetValidatedErrorField(err)
		c.ReplyFailWithDetail(lib.CodePara, detail)
		return false
	}

	//c.Context.Set(KEY_PARAMS, to)
	return true
}

func (c *XContext) GetReqObjectFromXml(to interface{}) (ok bool) {
	//c.GetTraceHeaders()
	//使用ShouldBindBodyWith 避免前置过滤器提前读取body，造成 EOF
	err := c.ShouldBindBodyWith(to, binding.XML)
	if err != nil {
		detail := c.GetValidatedErrorField(err)
		c.ReplyFailWithDetail(lib.CodePara, detail)
		return false
	}

	//c.Context.Set(KEY_PARAMS, to)
	return true
}

func (c *XContext) GetReqObjectWithErrInfo(to interface{}) (err error) {
	//c.GetTraceHeaders()
	err = c.ShouldBindBodyWith(to, binding.JSON)
	if err != nil {
		return
	}

	//c.Context.Set(KEY_PARAMS, to)
	return
}

// GetTraceHeaders 获取链路追踪的header存在xContext中，便于向上游传递
//func (c *XContext) GetTraceHeaders() {
//	for _, k := range lib.TraceHeaders {
//		v := c.Context.GetHeader(k)
//		if k == lib.XRequestID {
//			if v == "" {
//				v = uuid.NewV4().String()
//			}
//			c.Context.Set(k, v)
//		}
//		c.Set(k, v)
//	}
//}

// Reply reply msg
func (c *XContext) Reply(httpCode int, obj interface{}) {
	//c.Context.Set(KEY_RESULT, obj)

	if _, exists := GetRet(c.Context); !exists {
		SetRet(c.Context, lib.CodeOk)
	}

	c.JSON(httpCode, obj)
}

// ReplyPureJson reply msg with pure json
func (c *XContext) ReplyPureJson(httpCode int, obj interface{}) {
	//c.Context.Set(KEY_RESULT, obj)

	if _, exists := GetRet(c.Context); !exists {
		SetRet(c.Context, lib.CodeOk)
	}

	c.PureJSON(httpCode, obj)
}

func (c *XContext) ReplyOK(data interface{}) {
	resp := &XResp{
		Ret:  lib.CodeOk,
		Data: data,
	}
	SetRet(c.Context, lib.CodeOk)
	c.Reply(http.StatusOK, resp)
}

func (c *XContext) ReplyFail(code lib.Code) {
	resp := &XResp{
		Ret: code,
		Msg: lib.CodeMap[lib.Code(code)],
	}
	SetRet(c.Context, code)
	c.Reply(http.StatusOK, resp)
}

func (c *XContext) ReplyFailWithMsg(code lib.Code, msg string) {
	resp := &XResp{
		Ret: code,
		Msg: msg,
	}
	SetRet(c.Context, code)
	c.Reply(http.StatusOK, resp)
}

func (c *XContext) ReplyFailWithDetail(code lib.Code, detail string) {
	resp := &XResp{
		Ret:    code,
		Detail: detail,
		Msg:    lib.CodeMap[lib.Code(code)],
	}
	SetRet(c.Context, code)
	c.Reply(http.StatusOK, resp)
}

func (c *XContext) ReplyFailWithMsgAndDetail(code lib.Code, msg string, detail string) {
	resp := &XResp{
		Ret:    code,
		Detail: detail,
		Msg:    msg,
	}
	SetRet(c.Context, code)
	c.Reply(http.StatusOK, resp)
}

func (c *XContext) ReplyOKWithoutData() {
	resp := &XResp{
		Ret:  lib.CodeOk,
		Data: &struct{}{},
	}
	SetRet(c.Context, lib.CodeOk)
	c.Reply(http.StatusOK, resp)
}

func (c *XContext) Set(key string, value interface{}) {
	c.keysMutex.Lock()
	if c.XKeys == nil {
		c.XKeys = make(map[string]interface{})
	}
	c.XKeys[key] = value
	c.keysMutex.Unlock()
}

func (c *XContext) Get(key string) (value interface{}, exists bool) {
	c.keysMutex.RLock()
	value, exists = c.XKeys[key]
	c.keysMutex.RUnlock()
	return
}

func (c *XContext) Value(key interface{}) interface{} {
	if key == 0 {
		return c.Request
	}
	if keyAsString, ok := key.(string); ok {
		val, _ := c.Get(keyAsString)
		return val
	}
	return nil
}
