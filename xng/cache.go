package xng

import (
	"errors"
	"time"
)

type ICache interface {
	SetBytesEx(key string, data []byte, expire time.Duration) (err error)
	GetBytes(key string) (data []byte, ok bool)
	SetEx(key string, v interface{}, expire time.Duration) (err error)
	Get(key string, v interface{}) (ok bool)
	Del(key string) (err error)
}

type BaseCache struct {
	ICache
	Coder Codec
}

func (bc *BaseCache) SetCodec(codec Codec) {
	bc.Coder = codec
}

func (bc *BaseCache) Get(key string, v interface{}) (ok bool) {
	if bc.ICache == nil {
		return false
	}
	if bc.Coder == nil {
		//默认采用json编码器
		bc.Coder = NewJSONCodec()
	}
	data, ok := bc.ICache.GetBytes(key)
	if !ok || data == nil {
		return false
	}
	err := bc.Coder.Decode(data, v)
	if err != nil {
		return false
	}
	return true
}

func (bc *BaseCache) SetEx(key string, v interface{}, expire time.Duration) (err error) {
	if bc.ICache == nil {
		return errors.New("ICache is invalid")
	}
	if bc.Coder == nil {
		//默认采用json编码器
		bc.Coder = NewJSONCodec()
	}
	data, err := bc.Coder.Encode(v)
	if err != nil {
		return
	}
	err = bc.ICache.SetBytesEx(key, data, expire)
	return
}
