package xng

import (
	"gitlab.com/wangzhuan/lc"
	"time"
)

type LocalCache struct {
	BaseCache
}

func NewLC(codec Codec) *LocalCache {
	lc := &LocalCache{}
	lc.Coder = codec
	lc.ICache = lc
	return lc
}

func DefaultLC() *LocalCache {
	lc := &LocalCache{}
	lc.Coder = NewJSONCodec()
	lc.ICache = lc
	return lc
}

func (LC *LocalCache) SetBytesEx(key string, data []byte, expire time.Duration) (err error) {
	lc.Set(key, data, expire)
	return
}

func (LC *LocalCache) GetBytes(key string) (data []byte, ok bool) {
	val, ok := lc.Get(key)
	if ok {
		data = val.([]byte)
	}
	return
}

func (LC *LocalCache) Del(key string) (err error) {
	lc.Delete(key)
	return
}
