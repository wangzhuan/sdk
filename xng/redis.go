package xng

import (
	"errors"
	"github.com/garyburd/redigo/redis"
	"time"
)

var (
	BaseRDS map[string]*redis.Pool
)

func InitRedis(rds map[string]*redis.Pool) {
	BaseRDS = rds
}

type ExecFunction func(conn redis.Conn) (err error)

type IRedisAction interface {
	Exec(key interface{}, execFn ExecFunction) (err error)
	execWithConfig(key interface{}, execFn ExecFunction, config *RedisConfig) (err error)
	setBytesExWithConfig(key string, data []byte, expire time.Duration, config *RedisConfig) (err error)
	getBytesWithConfig(key string, config *RedisConfig) (data []byte, ok bool)
	delWithConfig(key string, config *RedisConfig) (err error)
}

type IRedisConfig interface {
	Db(key interface{}) string
}

type IRedis interface {
	IRedisConfig
	ICache
	IRedisAction
}

type RedisConfig struct {
	divideBy interface{}
	owner    IRedis
	BaseCache
}

type BaseRedis struct {
	IRedisConfig
	BaseCache
}

func (br *BaseRedis) Hash(v interface{}) *RedisConfig {
	rc := &RedisConfig{}
	rc.divideBy = v
	rc.owner = br
	rc.ICache = rc
	return rc
}

func (rc *RedisConfig) Exec(key interface{}, execFn ExecFunction) (err error) {
	if rc.owner == nil {
		return errors.New("owner of redis config is invalid")
	}

	return rc.owner.execWithConfig(key, execFn, rc)
}

func (rc *RedisConfig) SetBytesEx(key string, data []byte, expire time.Duration) (err error) {
	if rc.owner == nil {
		return errors.New("owner of redis config is invalid")
	}
	return rc.owner.setBytesExWithConfig(key, data, expire, rc)
}

func (rc *RedisConfig) GetBytes(key string) (data []byte, ok bool) {
	if rc.owner == nil {
		return nil, false
	}
	return rc.owner.getBytesWithConfig(key, rc)
}

func (rc *RedisConfig) Del(key string) (err error) {
	if rc.owner == nil {
		return errors.New("owner of redis config is invalid")
	}
	return rc.owner.delWithConfig(key, rc)
}

func (br *BaseRedis) SetRedisConfig(config IRedis) {
	br.IRedisConfig = config
	br.ICache = br
}

func (br *BaseRedis) Exec(key interface{}, execFn ExecFunction) (err error) {
	return br.execWithConfig(key, execFn, nil)
}

func (br *BaseRedis) execWithConfig(key interface{}, execFn ExecFunction, config *RedisConfig) (err error) {
	if br.IRedisConfig == nil {
		return errors.New("IRedisConfig is invalid")
	}

	var divideBy interface{}
	if config != nil {
		divideBy = config.divideBy
	}
	db := br.IRedisConfig.Db(br.Db(divideBy))

	if err != nil || db == "" {
		return errors.New("get redis db err")
	}
	pool := BaseRDS[db]
	conn := pool.Get()
	defer conn.Close()
	err = execFn(conn)
	return
}

func (br *BaseRedis) SetBytesEx(key string, data []byte, expire time.Duration) (err error) {
	return br.Exec(key, func(conn redis.Conn) (err1 error) {
		_, err1 = conn.Do("SETEX", key, int(expire), data)
		return
	})
}

func (br *BaseRedis) setBytesExWithConfig(key string, data []byte, expire time.Duration, config *RedisConfig) (err error) {
	return br.execWithConfig(key, func(conn redis.Conn) (err1 error) {
		_, err1 = conn.Do("SETEX", key, int(expire), data)
		return
	}, config)
}

func (br *BaseRedis) GetBytes(key string) (data []byte, ok bool) {
	br.Exec(key, func(conn redis.Conn) (err1 error) {
		v, errIgnore := redis.String(conn.Do("GET", key))
		if errIgnore != nil || v == "" {
			ok = false
			return
		}
		data = []byte(v)
		ok = true
		return
	})
	return
}

func (br *BaseRedis) getBytesWithConfig(key string, config *RedisConfig) (data []byte, ok bool) {
	br.execWithConfig(key, func(conn redis.Conn) (err1 error) {
		v, errIgnore := redis.String(conn.Do("GET", key))
		if errIgnore != nil || v == "" {
			ok = false
			return
		}
		data = []byte(v)
		ok = true
		return
	}, config)
	return
}

func (br *BaseRedis) Del(key string) (err error) {
	return br.Exec(key, func(conn redis.Conn) (err1 error) {
		_, err1 = conn.Do("DEL", key)
		return
	})
}

func (br *BaseRedis) delWithConfig(key string, config *RedisConfig) (err error) {
	return br.execWithConfig(key, func(conn redis.Conn) (err1 error) {
		_, err1 = conn.Do("DEL", key)
		return
	}, config)
}
