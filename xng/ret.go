/*
* @Author maxin(maxin@xiaoniangao.com)
* @Date 2020/12/2
* @Desc response ret定义了一套规范，同时各个业务也是可以扩展code值，这就带来了新的问题，无法直接识别这个code值是正常，异常，还是错误。最终的结果是无法衡量服务的稳定性以及进行报警策略配置。
 */

package xng

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/wangzhuan/sdk/lib"
)

type RetTranFunc func(lib.Code) lib.ServiceLevel

const (
	//RetKey 业务ret code原始的值
	RetKey = "response-ret"
	//ServiceLevelKey 服务级别，用来评估服务稳定性的关键指标
	ServiceLevelKey = "service-level"
)

// RetTranlate 中间件，ret与serviceLevel转换
func RetTranlate(tranFun RetTranFunc) gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Next()
		code, _ := GetRet(c)
		level := tranFun(code)
		if level > lib.LevelMax || level < lib.LevelMin {
			return
		}
		SetServiceLevel(c, level)
	}
}

func GetRet(c *gin.Context) (lib.Code, bool) {
	ncode, exists := c.Get(RetKey)
	if !exists {
		return 0, false
	}
	code, ok := ncode.(lib.Code)
	return code, ok
}

func SetRet(c *gin.Context, code lib.Code) {
	c.Set(RetKey, code)
}

func SetServiceLevel(c *gin.Context, level lib.ServiceLevel) {
	c.Set(ServiceLevelKey, level)
}

// GetServiceLevel nilToDefault 如果没有主动的设置serviceLevel，是否启用默认的转化方式
func GetServiceLevel(c *gin.Context, nilToDefault bool) lib.ServiceLevel {

	nlevel, exists := c.Get(ServiceLevelKey)
	if exists {
		level, ok := nlevel.(lib.ServiceLevel)
		if !ok {
			level = lib.LevelWarning
			return level
		}
		if level > lib.LevelMax || level < lib.LevelMin {
			level = lib.LevelWarning
		}
		return level
	}

	// 如果不存在的，并且不愿意设置默认转化
	if !nilToDefault {
		return lib.LevelNormal
	}

	// 如果不存在的，并且愿意默认转化

	ret, _ := GetRet(c)

	if ret == lib.CodeOk {
		return lib.LevelNormal
	}
	if ret == lib.CodeSrv || ret > lib.CodeMax {
		return lib.LevelError
	}
	return lib.LevelWarning
}
