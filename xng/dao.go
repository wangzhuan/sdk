package xng

import (
	"errors"
	mgo "gitlab.com/wangzhuan/github.com.globalsign.mgo"
	"gitlab.com/wangzhuan/github.com.globalsign.mgo/bson"
	"reflect"
)

var (
	BaseDBS    map[string]*mgo.Session
	genKeyFunc GenPrimaryKeyFunc
)

type BaseDao struct {
	IDaoConfig
}

func (bd *BaseDao) SetDaoConfig(config IDaoConfig) {
	bd.IDaoConfig = config
}

type DaoConfig struct {
	IsPrimaryPreferred bool
	Key                interface{}
}

func NewConfig(opts ...Option) *DaoConfig {
	config := &DaoConfig{}
	for _, opt := range opts {
		opt.Apply(config)
	}
	return config
}

type Option interface {
	Apply(*DaoConfig)
}

// 由于optionFunc 也实现了Apply方法，所以它也实现了Option接口
type optionFunc func(*DaoConfig)

func (f optionFunc) Apply(config *DaoConfig) {
	f(config)
}

func SetPrimaryPreferred() Option {
	return optionFunc(func(config *DaoConfig) {
		config.IsPrimaryPreferred = true
	})
}

func SetQueryKey(key interface{}) Option {
	return optionFunc(func(config *DaoConfig) {
		config.Key = key
	})
}

type IDaoConfig interface {
	Dsn(v interface{}) string
	DB(v interface{}) string
	Collection(v interface{}) string
}

type IDaoAction interface {
	AddOne(model interface{}, config *DaoConfig) (err error)
	AddWithAutoId(model interface{}, config *DaoConfig) (err error)
	UpdateById(id interface{}, model interface{}, config *DaoConfig) (err error)
	Update(selector interface{}, update interface{}, config *DaoConfig) (err error)
	UpdateAll(selector interface{}, update interface{}, config *DaoConfig) (err error)
	QueryOne(selector interface{}, ret interface{}, config *DaoConfig) (err error)
	QueryAll(selector interface{}, ret interface{}, config *DaoConfig) (err error)
	GetOne(id interface{}, ret interface{}, config *DaoConfig) (err error)
	DelOne(id interface{}, config *DaoConfig) (err error)
	ListByModel(model interface{}, limit int, offset int, ret interface{}, config *DaoConfig) (err error)
}

type ExecFunc func(c *mgo.Collection) (err error)
type ExecWithAutoIDFunc func(c *mgo.Collection, autoID int64) (err error)
type QueryFunc func(c *mgo.Collection) (err error)
type GenPrimaryKeyFunc func() (id int64)

func InitMgo(dbs map[string]*mgo.Session) {
	BaseDBS = dbs
}

// 用于初始化生成统一主键ID的方法
func InitGenKeyFunc(genFunc GenPrimaryKeyFunc) {
	genKeyFunc = genFunc
}

func (bd *BaseDao) Exec(execFn ExecFunc, config *DaoConfig) (err error) {
	c, err := bd.getC(config)
	if err != nil {
		return
	}

	defer c.Database.Session.Close()
	err = execFn(c)
	return
}

// 自动生成全局自增主键再注入
func (bd *BaseDao) ExecWithAutoID(execFn ExecWithAutoIDFunc, config *DaoConfig) (err error) {
	c, err := bd.getC(config)
	if err != nil {
		return
	}

	defer c.Database.Session.Close()
	if genKeyFunc == nil {
		return errors.New("gen primary key func is not found")
	}
	id := genKeyFunc()
	err = execFn(c, id)
	return
}

func (bd *BaseDao) getC(config *DaoConfig) (c *mgo.Collection, err error) {

	if BaseDBS == nil {
		return nil, errors.New("DBS invalid")
	}

	if bd.IDaoConfig == nil {
		return nil, errors.New("dao config is invalid")
	}

	var key interface{}
	if config != nil {
		key = config.Key
	}
	session := BaseDBS[bd.IDaoConfig.Dsn(key)]
	sessionCopy := session.Copy()
	if config != nil && config.IsPrimaryPreferred {
		sessionCopy.SetMode(mgo.PrimaryPreferred, true)
	}
	c = sessionCopy.DB(bd.IDaoConfig.DB(key)).C(bd.IDaoConfig.Collection(key))
	return
}

func (bd *BaseDao) AddOne(model interface{}, config *DaoConfig) (err error) {
	err = bd.Exec(func(c *mgo.Collection) error {
		return c.Insert(model)
	}, config)
	return
}

func (bd *BaseDao) AddWithAutoId(model interface{}, config *DaoConfig) (err error) {
	err = bd.ExecWithAutoID(func(c *mgo.Collection, autoID int64) error {
		err1 := setPrimaryKeyAuto(model, autoID)
		if err1 != nil {
			return err1
		}
		return c.Insert(model)
	}, config)
	return
}

func (bd *BaseDao) UpdateById(id interface{}, model interface{}, config *DaoConfig) (err error) {
	err = bd.Exec(func(c *mgo.Collection) error {
		return c.UpdateId(id, model)
	}, config)
	return
}

func (bd *BaseDao) Update(selector interface{}, update interface{}, config *DaoConfig) (err error) {
	err = bd.Exec(func(c *mgo.Collection) error {
		return c.Update(selector, update)
	}, config)
	return
}

func (bd *BaseDao) UpdateAll(selector interface{}, update interface{}, config *DaoConfig) (err error) {
	return bd.Exec(func(c *mgo.Collection) error {
		_, err1 := c.UpdateAll(selector, update)
		return err1
	}, config)
}

func (bd *BaseDao) QueryOne(selector interface{}, ret interface{}, config *DaoConfig) (err error) {
	err = bd.Exec(func(c *mgo.Collection) error {
		return c.Find(selector).One(ret)
	}, config)
	return
}

func (bd *BaseDao) QueryAll(selector interface{}, ret interface{}, config *DaoConfig) (err error) {
	err = bd.Exec(func(c *mgo.Collection) error {
		return c.Find(selector).All(ret)
	}, config)
	return
}

func (bd *BaseDao) GetOne(id interface{}, ret interface{}, config *DaoConfig) (err error) {
	err = bd.Exec(func(c *mgo.Collection) error {
		return c.FindId(id).One(ret)
	}, config)
	return
}

func (bd *BaseDao) DelOne(id interface{}, config *DaoConfig) (err error) {
	err = bd.Exec(func(c *mgo.Collection) error {
		return c.RemoveId(id)
	}, config)
	return
}

// 根据model的字段赋值来自动生成查询条件
func (bd *BaseDao) ListByModel(model interface{}, limit int, offset int, ret interface{}, config *DaoConfig) (err error) {
	err = bd.Exec(func(c *mgo.Collection) error {
		q := createQueryByModel(model)
		return c.Find(q).Skip(offset).Limit(limit).All(ret)
	}, config)
	return
}

func createQueryByModel(model interface{}) (q bson.M) {
	if model == nil {
		return
	}
	q = bson.M{}
	val := reflect.ValueOf(model).Elem()
	fType := reflect.TypeOf(model).Elem()
	for i := 0; i < val.NumField(); i++ {
		f := fType.Field(i)
		v := val.Field(i)
		if f.Type.Kind() == reflect.Int64 {
			vv := v.Interface().(int64)
			if vv > 0 {
				q[f.Tag.Get("bson")] = vv
			}
		} else if f.Type.Kind() == reflect.Int {
			vv := v.Interface().(int)
			if vv > 0 {
				q[f.Tag.Get("bson")] = vv
			}
		} else if f.Type.Kind() == reflect.String {
			vv := v.Interface().(string)
			if vv != "" {
				q[f.Tag.Get("bson")] = vv
			}
		}
	}
	return
}

func setPrimaryKeyAuto(model interface{}, id int64) error {

	val := reflect.ValueOf(model).Elem()
	fType := reflect.TypeOf(model).Elem()
	for i := 0; i < val.NumField(); i++ {
		f := fType.Field(i)
		v := val.Field(i)
		if f.Type.Kind() == reflect.Int64 &&
			f.Tag.Get("bson") == "_id" {
			v.Set(reflect.ValueOf(id))
			return nil
		}
	}
	return errors.New("primary id is invalid")
}
