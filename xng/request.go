package xng

import (
	"errors"
	"time"
)

type IRequest interface {
	Add(v interface{}) (err error)
	Get(v interface{}) (ok bool)
	Update(v interface{}) (err error)
	Del() (err error)
}

type CacheRequest struct {
	Key        string
	ExpireTime time.Duration
	Owner      ICache
}

func NewCacheRequest(key string, expire time.Duration, owner ICache) *CacheRequest {
	cr := &CacheRequest{}
	cr.Key = key
	cr.ExpireTime = expire
	cr.Owner = owner
	return cr
}

func (cr *CacheRequest) check() (err error) {
	if cr.Key == "" {
		return errors.New("key is invalid")
	}

	if cr.ExpireTime <= 0 {
		return errors.New("expire time is invalid")
	}

	if cr.Owner == nil {
		return errors.New("owner is invalid")
	}
	return
}

func (cr *CacheRequest) Add(v interface{}) (err error) {
	if v == nil {
		return errors.New("model is nil")
	}

	err = cr.check()
	if err != nil {
		return
	}

	err = cr.Owner.SetEx(cr.Key, v, cr.ExpireTime)
	return
}

func (cr *CacheRequest) Update(v interface{}) (err error) {
	if v == nil {
		return errors.New("model is nil")
	}

	err = cr.check()
	if err != nil {
		return
	}

	err = cr.Owner.SetEx(cr.Key, v, cr.ExpireTime)
	return
}

func (cr *CacheRequest) Get(v interface{}) (ok bool) {
	err := cr.check()
	if err != nil {
		return false
	}

	return cr.Owner.Get(cr.Key, v)
}

func (cr *CacheRequest) Del() (err error) {
	if cr.Key == "" {
		return errors.New("key is invalid")
	}

	if cr.Owner == nil {
		return errors.New("owner is invalid")
	}

	err = cr.Owner.Del(cr.Key)
	return
}

type DaoRequest struct {
	ID     interface{}
	Config *DaoConfig
	Owner  IDaoAction
}

func NewDaoRequest(id interface{}, owner IDaoAction, config *DaoConfig) *DaoRequest {
	dr := &DaoRequest{}
	dr.ID = id
	dr.Config = config
	dr.Owner = owner
	return dr
}

func (dr *DaoRequest) check() (err error) {
	if dr.ID == nil {
		return errors.New("id of dao request is invalid")
	}

	if dr.Owner == nil {
		return errors.New("owner of dao request is invalid")
	}

	return
}

func (dr *DaoRequest) Add(v interface{}) (err error) {
	if dr.Owner == nil {
		return errors.New("owner of dao request is invalid")
	}

	err = dr.Owner.AddOne(v, dr.Config)
	return
}

func (dr *DaoRequest) Update(v interface{}) (err error) {
	if dr.Owner == nil {
		return errors.New("owner of dao request is invalid")
	}

	err = dr.Owner.UpdateById(dr.ID, v, dr.Config)
	return
}

func (dr *DaoRequest) Get(v interface{}) (ok bool) {
	err := dr.check()
	if err != nil {
		return false
	}

	err = dr.Owner.GetOne(dr.ID, v, dr.Config)
	if err != nil {
		return false
	}
	return true
}

func (dr *DaoRequest) Del() (err error) {
	err = dr.check()
	if err != nil {
		return
	}

	err = dr.Owner.DelOne(dr.ID, dr.Config)
	return
}

type RequestGroup struct {
	RequestChain []IRequest
}

func NewRequestGroup() *RequestGroup {
	return &RequestGroup{RequestChain: make([]IRequest, 0)}
}

func (rg *RequestGroup) Use(reqs ...IRequest) {
	rg.RequestChain = append(rg.RequestChain, reqs...)
}

func (rg *RequestGroup) Add(v interface{}) (err error) {
	for i := 0; i < len(rg.RequestChain); i++ {
		err = rg.RequestChain[i].Add(v)
		if err != nil {
			return
		}
	}
	return
}

func (rg *RequestGroup) Next(index int, v interface{}) (ok bool) {
	req := rg.RequestChain[index]
	ok = req.Get(v)
	if !ok {
		index++
		if index < len(rg.RequestChain) {
			ok = rg.Next(index, v)
			if ok {
				req.Add(v)
			}
		}
	}
	return
}

func (rg *RequestGroup) Get(v interface{}) (ok bool) {
	if len(rg.RequestChain) <= 0 {
		return false
	}
	index := int(0)
	return rg.Next(index, v)
}

func (rg *RequestGroup) Update(v interface{}) (err error) {
	for i := 0; i < len(rg.RequestChain); i++ {
		err = rg.RequestChain[i].Update(v)
		if err != nil {
			return
		}
	}
	return
}

func (rg *RequestGroup) Del() (err error) {
	for i := 0; i < len(rg.RequestChain); i++ {
		err = rg.RequestChain[i].Del()
		if err != nil {
			return
		}
	}
	return
}
