package lib

import "gitlab.com/wangzhuan/sdk/grpc/resprpc"

// Code 用于定义返回码
type Code int

// 定义各种返回码
const (
	CodeOk                 = Code(resprpc.RetCode_Ok)
	CodeSrv                = Code(resprpc.RetCode_Srv)
	CodePara               = Code(resprpc.RetCode_Para)
	CodeRegister           = Code(resprpc.RetCode_Register)
	CodeSignIn             = Code(resprpc.RetCode_SignIn)
	CodeSignOut            = Code(resprpc.RetCode_SignOut)
	CodeSignCheck          = Code(resprpc.RetCode_SignCheck)
	CodeExist              = Code(resprpc.RetCode_Exist)
	CodeNotExist           = Code(resprpc.RetCode_NotExist)
	CodeUpdateErr          = Code(resprpc.RetCode_UpdateErr)
	CodeOccupied           = Code(resprpc.RetCode_Occupied)
	CodeDeletionNotAllowed = Code(resprpc.RetCode_DeletionNotAllowed)
	CodeStatusErr          = Code(resprpc.RetCode_StatusErr)
	CodeMax                = Code(resprpc.RetCode_Max)
)

// CodeMap 定义返回码对应的描述
var CodeMap = map[Code]string{
	CodeSrv:                "服务错误",
	CodePara:               "参数错误",
	CodeRegister:           "注册失败",
	CodeSignIn:             "登入失败",
	CodeSignOut:            "登出失败",
	CodeSignCheck:          "登陆校验失败",
	CodeExist:              "添加的信息已存在",
	CodeNotExist:           "对应信息不存在",
	CodeUpdateErr:          "部分信息更新失败",
	CodeOccupied:           "被占用",
	CodeDeletionNotAllowed: "不允许删除",
	CodeStatusErr:          "状态错误",
}
