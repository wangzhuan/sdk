package lib

const (
	// PROD 正式环境
	PROD = "prod"
	// DEV 开发环境，一般用于本机测试
	DEV = "dev"
	// TEST 测试环境
	TEST = "test"
	// PRE 预发布环境
	PRE = "pre"
)

// 请求头
const (
	XRequestID      = "x-request-id"
	XB3TraceID      = "x-b3-traceid"
	XB3SpanID       = "x-b3-spanid"
	XB3ParentSpanID = "x-b3-parentspanid"
	XB3Sampled      = "x-b3-sampled"
	XB3Flags        = "x-b3-flags"
	XOTSpanContext  = "x-ot-span-context"
)
//
const (
	TraceHeaderServerName = "serviceName"
)

var TraceHeaders = [...]string{
	XRequestID,
	XB3TraceID,
	XB3SpanID,
	XB3ParentSpanID,
	XB3Sampled,
	XB3Flags,
	XOTSpanContext,
}
