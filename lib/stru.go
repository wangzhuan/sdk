package lib

import (
	"encoding/json"
	"fmt"
	"time"
)

// Resp 用于定义返回数据格式(json)
type Resp struct {
	Ret    Code        `json:"ret"`
	Msg    string      `json:"msg,omitempty"`
	Detail string      `json:"detail,omitempty"`
	Data   interface{} `json:"data,omitempty"`
}

// Header 用于定义通用头
type Header struct {
	ID    int64    `json:"h_id"`
	Token string   `json:"h_token"`
	Roles []string `json:"h_roles"`
}

type WxHeader struct {
	ErrCode int    `json:"errcode"`
	ErrMsg  string `json:"errmsg"`
}

type TraceProc struct {
	Name string
	Dur  time.Duration
}

// Trace 用于定义trace信息
type Trace struct {
	ID      string
	Mid     int64
	SrvSrc  string
	SrvDst  string
	NameSrc string
	NameDst string
	Ts      int64
	Procs   []*TraceProc
}

func (trace *Trace) Encode() string {
	if trace == nil {
		return ""
	}
	return fmt.Sprintf("%s %d %s %s", trace.ID, trace.Mid, trace.SrvDst, trace.NameDst)
}

func (trace *Trace) Decode(s string) {
	if trace == nil {
		return
	}
	fmt.Sscanf(s, "%s%d%s%s", &trace.ID, &trace.Mid, &trace.SrvSrc, &trace.NameSrc)
}

func (trace *Trace) String() string {
	if trace == nil {
		return ""
	}

	trace.Ts = time.Now().UnixNano()

	bs, _ := json.Marshal(trace)
	return string(bs)
}

func (trace *Trace) FromString(s string) {
	if trace == nil {
		return
	}

	json.Unmarshal([]byte(s), trace)
}
