/*
* @Author maxin(maxin@xiaoniangao.com)
* @Date 2020/12/2
* @Desc 日志报警的级别
 */

package lib

type ServiceLevel int

const (
	LevelNormal  ServiceLevel = 1
	LevelError   ServiceLevel = 2
	LevelWarning ServiceLevel = 3
	LevelMax     ServiceLevel = LevelWarning
	LevelMin     ServiceLevel = LevelNormal
)
