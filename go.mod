module gitlab.com/wangzhuan/sdk

go 1.12

require (
	github.com/garyburd/redigo v1.6.0
	github.com/gin-gonic/gin v1.6.3
	github.com/go-playground/validator/v10 v10.2.0
	github.com/golang/protobuf v1.4.2 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/pkg/errors v0.8.1
	gitlab.com/wangzhuan/clog v1.2.2
	gitlab.com/wangzhuan/github.com.globalsign.mgo v1.1.1
	gitlab.com/wangzhuan/lc v1.1.1
	gitlab.com/wangzhuan/namecli v1.2.2
	gitlab.com/wangzhuan/utils v1.0.4
	go.uber.org/atomic v1.4.0 // indirect
	go.uber.org/multierr v1.1.0 // indirect
	go.uber.org/zap v1.10.0
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	google.golang.org/protobuf v1.23.0
)

replace golang.org/x/sys => github.com/golang/sys v0.0.0-20190405154228-4b34438f7a67
